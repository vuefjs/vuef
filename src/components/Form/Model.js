class Form {
    constructor(name) {
        this.pristine = true;
        this.name = name;
        this.fields = {};
    }
    addField(field) {
        this.fields[field.id] = field;
        return this;
    }
    addFields(fields) {
        fields.forEach(field => this.addField(field));
        return this;
    }
    exportValues() {
        const payload = {};
        Object.keys(this.fields).forEach((key) => {
            const field = this.fields[key];
            payload[field.id] = field.value;
        });
        return payload;
    }
    validate(...args) {
        return !Object.keys(this.fields)
            .map(key => this.fields[key])
            .filter(field => field.touched)
            .filter(field => !field.validate(...args)).length;
    }
}
export default Form;
