import rootStore from '../../store';
import * as mutationTypes from './mutation-types';

// mutations
const mutations = {
    [mutationTypes.SET_PROPERTY](state, payload) {
        state[payload.formname].fields[payload.propertyid].value = payload.value;
        state[payload.formname].fields[payload.propertyid].touch();
    },
    [mutationTypes.NEW_FORM](state, payload) {
        console.log('mutation NEW_FORM', payload.name);
        state[payload.name] = payload;
    },
    [mutationTypes.VALIDATE](state, payload) {
        console.log('mutation VALIDATE', payload);
    },
};

const store = {
    state: {},
    mutations,
};

rootStore.registerModule('form', store);

export default store;
