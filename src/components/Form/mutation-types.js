export const SET_PROPERTY = 'form/SET_PROPERTY';
export const NEW_FORM = 'form/NEW_FORM';
export const ADD_FORM_FIELD = 'form/ADD_FORM_FIELD';
export const ADD_FIELDSET = 'form/ADD_FIELDSET';
export const VALIDATE = 'form/VALIDATE';
