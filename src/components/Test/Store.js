import rootStore from '../../store';
import * as mutationTypes from './mutation-types';

// initial state
const state = {
    name: '',
    age: 18,
};

// mutations
const mutations = {
    [mutationTypes.SET_PROPERTY](state2, packge) {
        state2[packge.property] = packge.value;
    },
};

const store = {
    state,
    mutations,
};

rootStore.registerModule('test', store);

export default store;
