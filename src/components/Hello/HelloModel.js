class Hello {
    constructor({ id = '', greeting = '' } = {}) {
        this.id = id;
        this.greeting = greeting;
    }
    sayHi() {
        console.log(this.greeting);
    }
}

export default Hello;
