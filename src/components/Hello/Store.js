import rootStore from '../../store';
import * as mutationTypes from './mutation-types';

// initial state
const initialState = {
    hellos: [],
};

// mutations
const mutations = {
    [mutationTypes.ADD_NEW_HELLO](state, hello) {
        state.hellos.push(hello);
    },
    [mutationTypes.REMOVE_HELLO](state, hello) {
        const index = state.hellos.indexOf(hello);
        if (index !== -1) {
            state.hellos.splice(index, 1);
        }
    },
};

const store = {
    state: initialState,
    mutations,
};

rootStore.registerModule('hello', store);

export default store;
