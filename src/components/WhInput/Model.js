class WhInput {
    constructor(id, type, title, placeholder) {
        this.id = id;
        this.type = type;
        this.title = title;
        this.placeholder = placeholder;
        this.value = null;
        this.show = () => true;
        this.validations = [];

        this.touched = false;
        this.error = null;
    }
    setValue(value) {
        this.value = value;
        return this;
    }
    setShow(show) {
        this.show = show;
        return this;
    }
    addValidation(validation) {
        this.validations.push(validation);
        return this;
    }
    addValidations(validations) {
        validations.forEach(validation => this.addValidation(validation));
        return this;
    }
    touch() {
        this.touched = true;
    }
    validate(...args) {
        console.log('validating', this.id);
        const failures = this.validations
            .filter(validation => !validation.expression(...args))
            .map((validation) => {
                return (typeof validation.message === 'function') ?
                    validation.message(...args)
                    : validation.message;
            });
        this.error = (failures.length) ? failures[0] : null;
        return !failures.length;
    }
}

export default WhInput;
