class Validation {
    constructor(expression, message = '') {
        this.expression = expression;
        this.message = message;
    }
}

export default Validation;
