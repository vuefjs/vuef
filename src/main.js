import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';

import store from './store';
import routes from './routes';

Vue.use(VueRouter);
Vue.use(VueResource);

const router = new VueRouter({
    routes,
});

/* eslint-disable no-new */
new Vue({
    el: '#app',
    template: '<router-view></router-view>',
    components: {},
    store,
    router,
});
