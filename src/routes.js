import Home from './components/Home/Home';
import Hello from './components/Hello/Hello';
import Test from './components/Test/Test';

export default [
    {
        path: '/',
        title: 'Home',
        component: Home,
    },
    {
        path: '/hello',
        title: 'Hello',
        component: Hello,
    },
    {
        path: '/test',
        title: 'Test',
        component: Test,
    },
];
